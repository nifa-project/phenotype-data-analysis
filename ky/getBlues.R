# Getting BLUEs

library(lme4)
library(tidyverse)
library(emmeans)

setwd("~/workspace/phenotype-data-analysis/ky")

#source("extractDataFromExcel.R")

df <- readRDS("df.rds")
na_count <- sapply(df, function(x) sum(is.na(x)))
barplot(na_count[na_count < 4000])
yld_idx <- names(df)[grep("moist",names(df))]
na_count <- sapply(df[,yld_idx], function(x) sum(is.na(x)))
barplot(na_count)
sum(is.na(df$yldgrhl..adj.for..moist))
sum(is.na(df$yldgrhl.adj.for.moist))

boxplot_nonnum <- function(twtadj,rm.outliers=FALSE){
  twtadj <- as.numeric(twtadj)
  if(rm.outliers){
    bps <- boxplot.stats(twtadj)$out
    twtadj[twtadj %in% bps] <- NA 
  }
  boxplot(twtadj)
}

df$twtadj <- as.numeric(df$twtadj)
bps <- boxplot.stats(df$twtadj)$out
df$twtadj[df$twtadj %in% bps] <- NA
boxplot(df$twtadj)

df$new_yldadj <- as.numeric(df$new_yldadj)
boxplot(df$new_yldadj)
bps <- boxplot.stats(df$new_yldadj)$out
df$new_yldadj[df$new_yldadj %in% bps] <- NA
boxplot(df$new_yldadj)

df$yield <- as.numeric(df$yield)
boxplot(df$yield)
bps <- boxplot.stats(df$yield)$out
df$yield[df$yield %in% bps] <- NA
boxplot(df$yield)

df$new_hdate <- as.numeric(df$hdate)
boxplot(df$new_hdate)
bps <- boxplot.stats(df$new_hdate)$out
df$new_hdate[df$new_hdate %in% bps] <- NA
boxplot(df$new_hdate)

df$new_hdate <- as.numeric(df$hdate)
boxplot(df$new_hdate)
bps <- boxplot.stats(df$new_hdate)$out
df$new_hdate[df$new_hdate %in% bps] <- NA
boxplot(df$new_hdate)
sum(is.na(df$new_loc))
sum(is.na(df$year))

df$rating <- as.numeric(df$rating)
boxplot(df$rating)

fm1 <- lmer(yield ~ name + (1|year_loc), data = df)

blues.em <- emmeans(fm1, "name")
blues.df.em <- as.data.frame(blues.em)
blues.yld <- blues.df.em[,1:3]

fm2 <- lmer(twtadj ~ name + (1|year_loc), data = df)

getBlues <- function(model){
  blues.em <- emmeans(model, "name")
  blues.df.em <- as.data.frame(blues.em)
  blues <- blues.df.em[,1:3]
  return(blues)
}

blues.twt <- getBlues(fm2)

fm3 <- lmer(new_yldadj ~ name + (1|year_loc), data = df)
blues.yldadj <- getBlues(fm3)

fm4 <- lmer(new_hdate ~ name + (1|year_loc), data = df)
blues.hdate <- getBlues(fm4)

fm5 <- lmer(new_fhb ~ name + (1|year_loc), data = df)
blues.fhb <- getBlues(fm5)

colnames(blues.yld)[2:3] <- c("yld","yld.se")
colnames(blues.yldadj)[2:3] <- c("yldadj","yldadj.se")
colnames(blues.twt)[2:3] <- c("twt","twt.se")
colnames(blues.hdate)[2:3] <- c("hdate","hdate.se")
colnames(blues.fhb)[2:3] <- c("fhb","fhb.se")

mdf <- merge(blues.yld,blues.yldadj,by="name",all = T)
mdf <- merge(mdf,blues.twt,by="name",all = T)
mdf <- merge(mdf,blues.hdate,by="name",all = T)
mdf <- merge(mdf,blues.fhb,by="name",all = T)

write.csv(mdf,"ky_blues_se.csv")
write.csv(select(mdf,-grep(".se$",colnames(mdf))),"ky_blues.csv")