library(reshape2)
library(dplyr)


setwd("~/workspace/phenotype-data-analysis/NorGrains-2021/")


source("ptUtils.R")

inoh.t3 <- read.csv(file="data/t3/2022-04-21T191312phenotype_download.csv")
head(inoh.t3)
il.t3 <- read.csv(file="data/t3/2022-04-21T191341phenotype_download.csv")
head(il.t3)
ky.t3 <- read.csv("data/t3/KY2021_t3_traits_pheno.csv")
head(ky.t3)

# Correct IL germplasmName

awef <- grep("^([012])([0-9])-",il.t3[,"germplasmName"], value=T)
ViewExcel(awef)

awef <- grep("^202([01])-",il.t3[,"germplasmName"], value=T)
wtf(awef)

il.t3[,"germplasmName"] <- gsub("^([012])([0-9])-","IL\\1\\2-",il.t3[,"germplasmName"])
il.t3[,"germplasmName"] <- gsub("^202([01])-","IL2\\1-",il.t3[,"germplasmName"])

# Merge IN,OH with IL

compare.headers(il.t3,inoh.t3)
ilinoh.t3 <- merge(inoh.t3,il.t3,all = T)
dim(ilinoh.t3)
names(ilinoh.t3)

# Merge KY

compare.headers(ky.t3,ilinoh.t3)
head(ky.t3)

ky.t3$entryType <- c("test","check")[match(ky.t3$is_a_control,c("0","1"))]
old.ky.head <- c("plot_name","year","location","trial_name","design_type","breeding_program","description",
                 "planting_date","rep_number","col_number","row_number","plot_number","accession_name","weight_gram_seed_per_plot")
new.ky.head <- c("observationUnitName","studyYear","locationName","studyName","studyDesign","programName","studyDescription",
                 "plantingDate","replicate","colNumber","rowNumber","plotNumber","germplasmName","Grain.yield...g.plot.CO_321.0001221")

new.ky.names <- c(new.ky.head,names(ky.t3))[match(names(ky.t3),c(old.ky.head,names(ky.t3)))]
View(data.frame(new.ky.names,names(ky.t3)))
names(ky.t3) <- new.ky.names
ng.t3 <- merge(ilinoh.t3,ky.t3,all = T)
colSums(is.na(ng.t3))

# Get trait columns

traits <- grep("\\.CO",names(ng.t3),value=T)

# Remove plots without trait data

data.per.plot <- rowSums(!is.na(ng.t3[,traits]))
table(data.per.plot)
idx.has.dat <- data.per.plot>0
sum(idx.has.dat)
dim(ng.t3)
table(data.per.plot)["0"]+sum(idx.has.dat)
View(ng.t3[idx.has.dat,traits])

ng.nonmis.dat <- ng.t3[idx.has.dat,]

# Summarize trials per 

View(ng.nonmis.dat  %>%
  group_by(programName) %>%
  select(traits) %>%
  summarise_all(funs(sum(!is.na(.)))))

# Melt

str(ng.nonmis.dat)
mdcols <- c("studyYear","programName","studyName","locationName","germplasmName","replicate","rowNumber","colNumber","entryType")
premeltdf <- ng.nonmis.dat %>% select(c(mdcols,traits[1:4]))
#premeltdf[grep("^([012])([0-9])-(\\d+)$",premeltdf[,"germplasmName"],value=F),]
premeltdf[,"germplasmName"] <- gsub("^([012])([0-9])-(\\d+)$","IL\\1\\2-\\3",premeltdf[,"germplasmName"])
premeltdf[,mdcols] <- as.data.frame(lapply(premeltdf[,mdcols],as.factor))
md <- melt(premeltdf,na.rm=T)
md$studyName <- as.factor(gsub("^YR2-[123]","YR2",md$studyName))
md <- droplevels(md)
dim(md)
head(md)
save(md, file="output/melt_2021_plot_data.Rdata")

# Get with genotypes

source("~/workspace/genomic-prediction/src/gtutils.R")
gtd <- read.table("~/workspace/genomic-prediction/data/all_Norgrains_GBS_samps.txt", header = FALSE, sep = ":")

md.sn <- unique(md$germplasmName)
md.sn <- as.character(md.sn)

mn <- match.acc.names(md.sn,gtd$V5)
sum(!is.na(mn))

# sanity check, ifsome matching were not working well
md.sn[is.na(mn)]
grep("LCSDH",gtd$V5,value=T)
grep("21PU",gtd$V5,value=T)

# write samples out
gtd$V6 <- md.sn[match.acc.names(gtd$V5,md.sn)]
write.table(gtd %>% filter(!is.na(V6)) %>% select(1:5),"output/2021_lines_with_gt.txt", row.names = F, col.names = F, quote = F, sep = ":")

