library(readxl)
library(lme4)
library(tidyverse)
library(reshape2)

setwd("~/workspace/phenotype-data-analysis/NorGrains-2021")

# Load IL phenotypes
load("data/TrainingSetJul13.2021.RData")
il_all <- All
il_all$program <- "IL"

# Rename IL lines
gn <- as.character(il_all$germplasmName)
gn2 <- gsub("\\s","",gn) # remove white space
gn2 <- gsub("_","-",gn2) # replace underscore with hyphen
gn2 <- toupper(gn2) # make all uppercase
gn2 <- gsub("^([0-9][0-9])-","IL\\1-",gn2) # add IL prefix
il_all$germplasmName <- as.factor(gn2)

# Function to calculate IL BLUPs 
data <- il_all
trt <- "DON"
mm <- function(trt, data){
  print(trt)
  dat <- data %>% filter(trait == trt)
  if(trt %in% c("Yield","TestWeight", "Height")){
    # For Yield, test weight, height
    il_mm <- lmer(data=dat, predicted.value ~ (1|loc) + (1|year) + (1|site) + (1|study) + (1| germplasmName) + (1|germplasmName:site))
  }else{
    # For other traits
    il_mm <- lmer(data=dat, predicted.value ~ (1|year) + (1|study) + (1| germplasmName) + (1|germplasmName:year)) 
  }
  ce <- coef(il_mm)
  blups <- ce$germplasmName
  blups$NAME <- rownames(blups)
  return(blups %>% select(2,1))
}

mm21 <- function(trt, data){
  print(trt)
  dat <- data %>% filter(trait == trt)
  if(trt %in% c("Yield","TestWeight", "Height")){
    # For Yield, test weight, height
    il_mm <- lmer(data=dat, predicted.value ~ (1|loc) + (1|site) + (1|study) + (1| germplasmName) + (1|germplasmName:site))
  }else{
    # For other traits
    il_mm <- lmer(data=dat, predicted.value ~ (1|study) + (1| germplasmName)+ (1| germplasmName)) 
  }
  ce <- coef(il_mm)
  blups <- ce$germplasmName
  blups$NAME <- rownames(blups)
  return(blups %>% select(2,1))
}

# Get IL BLUPs
traits <- unique(il_all$trait)
il_blups <- lapply(traits,mm,il_all)
il_all21 <- il_all %>% filter(year==21)
traits21 <- unique(il_all21$trait)
il_blups21 <- lapply(traits21,mm21,il_all21)
lapply(il_blups,head)

il_df <- il_blups %>% reduce(full_join, by="NAME")
colnames(il_df) <- toupper(c("NAME", paste0("IL.",traits)))
head(il_df)


il_df21 <- il_blups21 %>% reduce(full_join, by="NAME")
colnames(il_df21) <- toupper(c("NAME", paste0("IL.",traits21)))
head(il_df21)


system("mkdir -p output")

# Load OH phenotypes
oh_blues <- read_excel("data/2021 BLUES IL-IN-KY-OH Lines.xlsx",2)
colnames(oh_blues) <- c("NAME","OH.YLD", "OH.TW")

# Load IN phenotypes
in_pheno <- read_excel("data/2021 Y1 data for predictions.xlsx",1)
colnames(in_pheno) <- toupper(paste0("IN.",colnames(in_pheno)))
colnames(in_pheno)[1:2] <- c("TEST","NAME")

# Load KY phenotypes
ky_files <- list.files("data/KY", full.names = T)
ky_pheno <- lapply(ky_files,read_excel,1)
ky_pheno[[1]] <- ky_pheno[[1]] %>% select(1:13)
ky_study_names <- gsub("^.*/(.*)LSM_KY.xlsx$","\\1",ky_files)
names(ky_pheno) <- ky_study_names
lapply(ky_pheno,names)
names(ky_pheno[[2]])[1] <- "NAME"
names(ky_pheno[[1]])[5] <- "TEST"

remove.na.cols <- function(x){
  return(x[!colSums(is.na(x)) == nrow(x)])
}
ky_pheno <- lapply(ky_pheno, remove.na.cols)
names(ky_pheno[[1]])
mt1 <- melt(ky_pheno[[1]], id=c("YEAR","LOC","ENTRY","NAME","TEST"), na.rm=TRUE)
mt2 <- melt(ky_pheno[[2]], id=c("NAME","TEST"), na.rm=TRUE)
mt3 <- melt(ky_pheno[[3]], id=c("YEAR","LOC","ENTRY", "counter","COMMENT","NAME","TEST"), na.rm=TRUE)
mt4 <- melt(ky_pheno[[4]], id=c("TEST","Entry","NAME"),na.rm=TRUE)
mt5 <- melt(ky_pheno[[5]], id=c("YEAR","LOC","ENTRY","NAME","TEST"), na.rm=TRUE)
cols <- c("TEST", "NAME", "variable", "value")
merged_ky <- rbind(mt1[cols],mt2[cols],mt3[cols],mt4[cols],mt5[cols])
ky_lsm <- merged_ky %>% slice(grep("^Mean", variable, ignore.case=T,invert = T)) %>% slice(grep("^PERCLOC", variable, invert=T))

trait_name <- "yield"
data <- ky_lsm$variable
replace_trait_names <- function(trait_name, data, replacement=NULL) {
  if(is.null(replacement))
    replacement <- trait_name
  gsub(paste0(".*",trait_name,".*"),toupper(paste("KY",replacement,sep=".")),data,ignore.case = T)
}
ky_traits <- c("yield","tstwght","hdate","height")
ky_lsm$trait <- ky_lsm$variable
for(trt in ky_traits){
  ky_lsm$trait <- replace_trait_names(trt,ky_lsm$trait)
}
ky_lsm$trait <- replace_trait_names("TWT",ky_lsm$trait,"tstwght")
ky_traits <- unique(ky_lsm$trait)

ky_trt <- ky_traits[1]

ky_mm <- function(ky_trt, dat){
  dat <- ky_lsm %>% filter(trait == ky_trt)
  dat$value <- as.numeric(dat$value)
  ky_mm <- lmer(data=dat, value ~ (1|TEST) + (1| NAME))
  summary(ky_mm)
  blups <- coef(ky_mm)$NAME
  blups$NAME <- rownames(blups)
  return(blups %>% select(2,1))
}

ky_blups <- lapply(ky_traits,ky_mm)
ky_df <- ky_blups %>% reduce(full_join, by="NAME")
names(ky_df) <- c("NAME", ky_traits)
ky_df
write.csv(ky_df,"output/2021_ky_blups.csv")
write.csv(il_df21,"output/2021_il_blups.csv")
write.csv(il_df,"output/2015-21_il_blups.csv")
write.csv(oh_blues,"output/2021_oh_blues.csv")
write.csv(in_pheno,"output/2021_in_blues.csv")

list_pheno <- list(ky_df,il_df21,in_pheno %>% select(-1),oh_blues)
merged <- list_pheno %>% reduce(full_join, by="NAME")
write.csv(merged,"output/2021_all_NIFA_blues_blups.csv")

# Include MI pheno
mi_blues <- read.csv("data/2021breedingPheno.csv")
head(mi_blues)
dim(mi_blues)
colSums(is.na(mi_blues))
mi_blues <- mi_blues[rowSums(is.na(mi_blues)) < dim(mi_blues)[2]-1,] 
dim(mi_blues)
View(mi_blues)
names(mi_blues) <- paste("MI",toupper(names(mi_blues)),sep = ".")
names(mi_blues)[1] <- "NAME"

merged2 <- full_join(merged,mi_blues,by="NAME")
write.csv(merged2,"output/2021_NorGrains_blues_blups.csv")
